/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test.chainresponsibility.impl;

import com.mycompany.test.chainresponsibility.IAprobador;

/**
 *
 * @author anavarretev
 */
public class EjecutivoDeCuenta implements IAprobador{
    
    private IAprobador next;
    
    @Override
    public IAprobador getNext() {
        return next;
    }
    
    @Override
    public void setNext(IAprobador aprobador) {
        next = aprobador;
    }

    @Override
    public void solicitudPrestamo(int monto) {
        
        if(monto == 1000){
            System.out.println("EjecutivoDeCuenta");
        } else {
            next.solicitudPrestamo(monto);
        }
    }
}
